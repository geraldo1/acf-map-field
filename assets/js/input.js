(function($){
	
	
	/**
	*  initialize_field
	*
	*  This function will initialize the $field.
	*
	*  @date	30/11/17
	*  @since	5.6.5
	*
	*  @param	n/a
	*  @return	n/a
	*/

	var map, iconGeometry, inputLat, inputLng, inputCountry;
	
	function initialize_field( $field ) {
		
		// Get var from this file
        const field_key = 'acf-' + $field[0].dataset.key,
        	  fieldElt = $('#' + field_key).get(0),
        	  zoom = fieldElt.getAttribute('data-zoom'),
        	  center_lat = fieldElt.getAttribute('data-center-lat'),
        	  center_lng = fieldElt.getAttribute('data-center-lng'),
        	  inputLatACF = '.map-field-input-lat input',
        	  inputLngACF = '.map-field-input-lng input',
        	  inputLatCustom = 'input.map-field-input-lat',
        	  inputLngCustom = 'input.map-field-input-lng',
        	  inputCountryACF = '.map-field-input-country select',
        	  inputCountryCustom = 'select.map-field-input-country';
        var lat = 0,
        	lng = 0,
        	coord,
        	center;

        if ($(inputLatACF).length && $(inputLngACF).length) {
	        // lat and lng fields are part of ACF coordinate field
        	lat = $(inputLatACF).val();
        	lng = $(inputLngACF).val();
        	inputLat = inputLatACF;
        	inputLng = inputLngACF;
        	inputCountry = inputCountryACF;
        }
        else if ($(inputLatCustom).length && $(inputLngCustom).length) {
        	// lat and lng fields are WP custom fields
			lat = $(inputLatCustom).val();
            lng = $(inputLngCustom).val();
        	inputLat = inputLatCustom;
        	inputLng = inputLngCustom;
        	inputCountry = inputCountryCustom;
        }
        else {
        	console.log("Error in latitude and longitude field configuration, have a look at documentation. Probably you didn't make latitude and longitude fields?");
        }
        coord = Array(parseFloat(lng), parseFloat(lat));

        if (isNumber(coord[0]) && isNumber(coord[1])) {
        	// set center and marker to saved position
        	center = coord;
        }
        else {
        	// set center to default center if not set and marker outside
        	center = [center_lng, center_lat];
        	coord = [0,0];
        }

		// initialize openlayers map
        center = ol.proj.fromLonLat(center);
        coord = ol.proj.fromLonLat(coord);

		iconGeometry = new ol.geom.Point(coord);

		var iconFeature = new ol.Feature({
            geometry: iconGeometry,
            name: 'POI'
        });

        var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(({
                anchor: [0.5, 36],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 0.75,
                src: '../wp-content/plugins/acf-map-field/assets/images/marker.png'
            }))
        });

        iconFeature.setStyle(iconStyle);

        var iconLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
	            features: [iconFeature]
	        })
        });

        var wmsLayer = new ol.layer.Tile({
            //source: new ol.source.OSM()
		    source: new ol.source.XYZ({ 
		        url:'http://{1-4}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
		    })
        });

		map = new ol.Map({
	        target: 'backend_map',
            layers: [wmsLayer, iconLayer],
	        view: new ol.View({
				center: center,
				zoom: zoom
	        })
	    });

	    // add countries layer
	    let countriesUrl = '../wp-content/plugins/acf-map-field/assets/js/countries.geojson',
	    	countriesSource = null;
	    if (inputCountry) {
		    $.get(countriesUrl)
		    .done(function() { 
		        // geojson does exist
		        countriesSource = new ol.source.Vector({
			        url: countriesUrl,
			        format: new ol.format.GeoJSON()
		    	});

		        map.addLayer(new ol.layer.Vector({
			    	source: countriesSource
			    }));
			});
		}

        map.on('singleclick', function (evt) {
            iconGeometry.setCoordinates(evt.coordinate);
            var coord = ol.proj.toLonLat(evt.coordinate);
            
            // set inputs to clicked coordinate
            $(inputLat).val(coord[1]);
            $(inputLng).val(coord[0]);

            // preselect country field
            if (countriesSource) {
            	let countries = countriesSource.getFeaturesAtCoordinate(evt.coordinate);
            	let country = countries[0].get("ADM0_A3");
            	//console.log(country.toLowerCase(), countries[0].get("ADMIN"));

            	const apiUrl = "http://300mil.net//wp-json/wp/v2/pais?slug=";

            	$.getJSON( apiUrl + country.toLowerCase(), function( data ) {

            		if (data.length > 0 && data[0].hasOwnProperty("id")) {
            			let countryId = data[0].id;
            			//console.log(countryId);
	            		//$(inputCountry).val(countryId).trigger('change');

						let newOption = $("<option selected='selected'></option>").val(countryId).text(countries[0].get("ADMIN"));
						$(inputCountry).append(newOption).trigger('change');
	            	}
            	});
            }
        });

        $(inputLat).on('input', function(e) {
        	setMarkerPos();
        })

        $(inputLng).on('input', function(e) {
        	setMarkerPos();
        })
	}

	function setMarkerPos() {

		if (!isNaN($(inputLat).val()) && !isNaN($(inputLng).val())) {

	        var lat = $(inputLat).val(),
	            lng = $(inputLng).val(),
	            coord = ol.proj.fromLonLat([lng,lat]);

	    	iconGeometry.setCoordinates(coord);

	    	// set map center
	    	map.getView().setCenter(coord);
	    }
	}
	
	
	if( typeof acf.add_action !== 'undefined' ) {
	
		/*
		*  ready & append (ACF5)
		*
		*  These two events are called when a field element is ready for initizliation.
		*  - ready: on page load similar to $(document).ready()
		*  - append: on new DOM elements appended via repeater field or other AJAX calls
		*
		*  @param	n/a
		*  @return	n/a
		*/
		
		acf.add_action('ready_field/type=map_field', initialize_field);
		acf.add_action('append_field/type=map_field', initialize_field);
		
		
	} else {
		
		/*
		*  acf/setup_fields (ACF4)
		*
		*  These single event is called when a field element is ready for initizliation.
		*
		*  @param	event		an event object. This can be ignored
		*  @param	element		An element which contains the new HTML
		*  @return	n/a
		*/
		
		$(document).on('acf/setup_fields', function(e, postbox){
			
			// find all relevant fields
			$(postbox).find('.field[data-field_type="map_field"]').each(function(){
				
				// initialize
				initialize_field( $(this) );
				
			});
		
		});
	
	}

	var isNumber = function isNumber(value) {
		return typeof value === 'number' && isFinite(value);
	}

})(jQuery);
