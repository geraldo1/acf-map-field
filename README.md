# ACF Map Field

This plugin offers a Openlayers map for the Wordpress backend. It is the perfect solution if you want to show one coordinate with (custom) posts. Using this field you can show and set a location on a map. It uses external latitude and longitude fields, either custom wordpress fields or ACF fields.

Clicking on the map sets the marker position and shows it in the latitude and longitude text fields. Editing latitude and longitude text inputs moves the marker position.

## Installation

To make this field work you have to do one of the following:
1. Define a ACF Group called `coordinates` with numeric subfields called `latitude` and `longitude`.
2. Define Wordpress custom input fields named `latitude` and `longitude`.

In both cases you have to define classes for the wrapper atribute, `map-field-input-lat` for latitude and `map-field-input-lng` for longitude field:
```
	'name' => 'latitude',
	'wrapper' => array(
		`'class' => 'map-field-input-lat',
```

```
	'name' => 'latitude',
	'wrapper' => array(
		'class' => 'map-field-input-lng',
```

Optionally you can define a ACF field called a `country` field with `map-field-input-country` as class name for the wrapper atribute. In this case a country polygons layer is loaded into the backend map and by clicking on the map the this country field gets filled by its name.

Optionally you also can define a ACF text field called `poligono`, in this case the backend map also paints the polygon.

## Usage

Using `get_field()` method on front end template outputs an Openlayers map with the marker on the coordinates position.

Initial map center and zoom value can be set in the field properties.

## Comparision

Differences to other ACF backend maps:
- [ACF Google field](https://www.advancedcustomfields.com/resources/google-map/) offers a complete solution to provide autocomplete searching, reverse geocoding lookup and an interactive marker. It saves street_number, street_name, city, state, post_code and country together with the coordinates. It depends on Google Maps JS API and so needs an API key from Google.
- [ACF OpenStreetMap Field](https://wordpress.org/plugins/acf-openstreetmap-field/) is a solution for using OpenStreetMap in backend and frontend. It uses Leaflet to draw the map and saves the one or multiple locations with every map.

_ACF Map Field_ pretends to be an minimalistic approach for showing wordpress content on a map. It should be used when the other solutions seem to be an overkill and you simply want to show one latitude and longitude with every (custom) post.

## Changelog

- 1.1: Add optional country field
- 1.0.1: Update Openlayers to latest release 7.3
- 1.0.0: Initial Release.