=== Advanced Custom Fields: Map Field ===
Contributors: Gerald Kogler
Tags: acf, openlayers, ol, map, location, latitude, longitude, poi
Requires at least: 6.0
Tested up to: 6.2
Stable tag: trunk
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin offers a Openlayers map for the Wordpress backend. It is the perfect solution if you want to show one coordinate with (custom) posts. Using this field you can show and set a location on a map. It uses external latitude and longitude fields, either custom wordpress fields or ACF fields.

Clicking on the map sets the marker position and shows it in the latitude and longitude text fields. Editing latitude and longitude text inputs moves the marker position.

== Requirements ==

To make this field work you have to do one of the following:
1. Define a ACF Group called `coordinates` with numeric subfields called `latitude` and `longitude`.
2. Define Wordpress custom input fields named `latitude` and `longitude`.

In both cases you have to define classes for the wrapper atribute, `map-field-input-lat` for latitude and `map-field-input-lon` for longitude field.

Additionally you can define a ACF text field called `poligono`, in this case the backend map also paints the polygon.

= Usage =

Using `get_field()` method on front end template outputs an Openlayers map with the marker on the coordinates position.

Initial map center and zoom value can be set in the field properties.

= Compatibility =

This ACF field type is compatible with ACF 6

== Installation ==

1. Copy the `acf-map-field` folder into your `wp-content/plugins` folder
2. Activate the Map Field plugin via the plugins admin page
3. Create a new field via ACF and select the Map Field type
4. Read the usage instructions above

== Changelog ==

= 1.1 =
* Add optional country field
= 1.0.1 =
* Update Openlayers to latest release 7.3
= 1.0.0 =
* Initial Release.